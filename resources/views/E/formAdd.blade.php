<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Add</title>
</head>
<body>
    <h2>Form Add</h2>
    <br>

    <a href="/projects"><button class="btn-back"><strong>Kembali<strong></button></a>
    <br>

    <form action="/projects/create" method="POST">
    <!-- Instan Security laravel  -->
    {{ csrf_field() }}
    <table>
    
        Nama : <input type="text" name="nama">
        @if($errors->has('nama'))
            {{ $errors->first('nama') }}
        @endif 
        <br>

        Tanggal Mulai : <input type="dateTime" value="<?php echo date ("Y-m-d \ H:i:s"); ?>" name="tanggal_mulai">
        @if($errors->has('tanggal_mulai'))
            {{ $errors->first('tanggal_mulai') }}
        @endif
        <br>

        Tanggal Target : <input type="dateTime" value="<?php echo date ("Y-m-d \ H:i:s"); ?>" name="tanggal_target"> 
        @if($errors->has('tanggal_target'))
            {{ $errors->first('Tanggal_target') }}
        @endif
        <br>

        Tanggal Selesai : <input type="dateTime" value="<?php echo date ("Y-m-d \ H:i:s"); ?>" name="tanggal_selesai"> 
        @if($errors->has('tanggal_selesai'))
            {{ $errors->first('tanggal_selesai') }}
        @endif
        <br>

        <button type="submit">Simpan</button>
    </table>
    </form>
</body>
</html>
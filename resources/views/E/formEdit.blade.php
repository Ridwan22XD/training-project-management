<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Edit</title>
</head>
<body>
    <h2>Form Edit</h2>
    <br>

    <a href="/projects"><button class="btn-back"><strong>Kembali<strong></button></a>
    <br>

    <form action="/projects/{projectId}" method="POST">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <input type="hidden" name="id" value="{{ $projects -> id }}">
        Nama <input type="text" name="nama" required="required" value="{{ $projects -> nama }}">
        @if($errors->has('nama'))
            {{ $errors->first('nama') }}
        @endif  <br>

        Tanggal Mulai <input type="datetime" name="tanggal_mulai" required="required" value="{{ $projects -> tanggal_mulai }}">
        @if($errors->has('nama'))
            {{ $errors->first('nama') }}
        @endif  <br>

        Tanggal Target <input type="datetime" name="tanggal_target" required="required" value="{{ $projects -> tanggal_target }}"> 
        @if($errors->has('nama'))
            {{ $errors->first('nama') }}
        @endif  <br>

        Tanggal Selesai <input type="datetime" name="tanggal_selesai" required="required" value="{{ $projects -> tanggal_selesai }}">
        @if($errors->has('nama'))
            {{ $errors->first('nama') }}
        @endif  <br>

        <input type="submit" value="Simpan" class="btn-save">
    
    </form>
</body>
</html>
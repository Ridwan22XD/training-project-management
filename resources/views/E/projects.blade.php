<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>List</title>
</head>
<body>
    <h2>List Using Eloquent</h2>
    <br>

    <a href="/projects"><button class="btn-back"><strong>tambah<strong></button></a>
    <br>

    <table border="1">
        <tr>
            <td>Id</td>
            <td>Nama</td>
            <td>Tanggal_Mulai</td>
            <td>Tanggal_target</td>
            <td>Tanggal_selesai</td>
            <td>Opsi</td>
        </tr>
        @foreach($projects as $project)
        <tr>
            <td>{{ $project->id }}</td>
            <td>{{ $project->nama }}</td>
            <td>{{ $project->tanggal_mulai }}</td>
            <td>{{ $project->tanggal_target }}</td>
            <td>{{ $project->tanggal_target }}</td>
            <td>
                <a href="/projects/{{ $project->id }}"><button class="btn-edit">Edit</button></a>
                <a href="/projects/{{ $project->id }}/delete"><button class="btn-delete">Delete</button></a>
            </td>
        </tr>
        @endforeach
    </table>

</body>
</html>
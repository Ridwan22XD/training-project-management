<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <title>Form Update</title>
</head>
<body>
    <h2>Update Data</h2>

    <br>

    <a href="/projects"><button class="btn-back"><strong>Kembali</strong></button></a>

    @foreach($projects as $project)
    {{ dd($projects) }}
        <form action="/projects/{projectId}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $project -> id }}"> <br>
            Nama <input type="text" name="nama" required="required" value="{{ $project -> nama }}"> <br>
            Tanggal Mulai <input type="datetime" name="tanggal_mulai" required="required" value="{{ $project -> tanggal_mulai }}"> <br>
            Tanggal Target <input type="datetime" name="tanggal_target" required="required" value="{{ $project -> tanggal_target }}"> <br>
            Tanggal Selesai <input type="datetime" name="tanggal_selesai" required="required" value="{{ $project -> tanggal_selesai }}"> <br>
            <input type="submit" value="Simpan" class="btn-save">
        
        </form>
    @endforeach
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <title>Daftar Project</title>
</head>
<body>
    <h2>Daftar Project</h2>

    <br>

    <!-- Internal CSS  -->
    <style type="text/css">
        .pagination li {
            float: left;
            list-style-type: none;
            margin: 5px;
        }
    </style>

    <a href="/projects/create"><button class="btn-add"><strong>Tambah</strong></button></a>
    <table border='1'>
        <tr>
            <td>Id</td>
            <td>Nama</td>
            <td>Tanggal Mulai</td>
            <td>Tanggal Target</td>
            <td>Tanggal Selesai</td>
            <td>Option</td>
        </tr>
        @foreach($projects as $project)
        <tr>
            <td>{{ $project->id }}</td>
            <td>{{ $project->nama }}</td>
            <td>{{ $project->tanggal_mulai }}</td>
            <td>{{ $project->tanggal_target }}</td>
            <td>{{ $project->tanggal_selesai }}</td>
            <td>
                <a href="/projects/{{ $project -> id }}" ><button class="btn-edit"><strong>Edit</strong></button></a>
                <a href="/projects/{{ $project -> id }}/delete" ><button class="btn-delete"><strong>Delete</strong></button></a>
            </td>
        </tr>
        @endforeach
    </table>

    <br>
    <!-- Halaman yang sedang dibuka -->
    Halaman : {{ $projects->currentPage() }} <br>
    <!-- Jumlah data yang ada  -->
    Jumlah Data : {{ $projects->total() }} <br>
    <!-- Data yang ditampilkan -->
    Data/Halaman : {{ $projects->perPage() }} <br>

    <!-- Membuat link penomoran -->
    {{ $projects->links() }} <br>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <title>Form Create Project</title>
</head>
<body>
    <h2>Tambah Project</h2>
    
    <br>

    <a href="/projects"><button class="btn-back"><strong>Kembali<strong></button></a>

    <br>

    <form action="/projects/create" method="POST">
        {{ csrf_field() }}
        Nama <input type="text" name="nama" required="required"> <br>
        Tanggal Mulai <input type="dateTime" name="tanggal_mulai" required="required" value="<?php echo date ("Y-m-d \ H:i:s"); ?> "><br>
        Tanggal Target <input type="datetime" name="tanggal_target" required="required" value="<?php echo date ("Y-m-d \ H:i:s") ?>"> <br>
        Tanggal Selesai <input type="datetime" name="tanggal_selesai" required="required" value="<?php echo date ("Y-m-d \ H:i:s") ?>"> <br>
        <input type="submit" value="Simpan Data">
    </form>
</body>
</html>
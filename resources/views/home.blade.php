@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <h2>List Using Eloquent</h2>
    <br>

    <a href="/projects/create"><button class="btn-add">Tambah</button></a>
    <br>
    <br>

    <table border="2">
        <tr>
            <td>Id</td>
            <td>Nama</td>
            <td>Tanggal_Mulai</td>
            <td>Tanggal_target</td>
            <td>Tanggal_selesai</td>
            <td>Opsi</td>
        </tr>
        @foreach($projects as $project)
        <tr>
            <td>{{ $project->id }}</td>
            <td>{{ $project->nama }}</td>
            <td>{{ $project->tanggal_mulai }}</td>
            <td>{{ $project->tanggal_target }}</td>
            <td>{{ $project->tanggal_target }}</td>
            <td>
                <a href="/projects/{{ $project->id }}"><button>Edit</button></a>
                <a href="/projects/{{ $project->id }}/delete"><button>Delete</button></a>
            </td>
        </tr>
        @endforeach
    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// // Daftar Project
// Route::get('/projects', 'ProjectController@index');
// // View: projects.index
 
// // Tampilan tambah Project
// Route::get('/projects/create', 'ProjectController@showCreate');
// // View: projects.create
 
// // Post tambah Project
// Route::post('/projects/create', 'ProjectController@create');
// // Redirect ke /projects
 
// // Tampilan update Project
// Route::get('/projects/{projectId}', 'ProjectController@showUpdate');
// // View: projects.update
 
// // Post update Project
// Route::post('/projects/{projectId}', 'ProjectController@update');
// // Redirect ke /projects/{projectId}
 
// // Post delete Project
// Route::get('/projects/{projectId}/delete', 'ProjectController@delete');
// // Redirect ke /projects


// Eloquent

Route::get('/','ProjectController@login');

// index Project 
Route::get('/list','ProjectController@index');

// Form Add Project 
Route::get('/projects/create','ProjectController@formAdd');
// Adding data in database
Route::post('/projects/create','ProjectController@add');

// Form Edit
Route::get('/projects/{projectId}','ProjectController@formEdit');
// Confirm Edit
Route::put('/projects/{projectId}','ProjectController@edit');

// Delete Project
Route::get('/projects/{projectID}/delete','ProjectController@delete');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

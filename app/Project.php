<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    // Select table project in database
    protected $table = "projects";

    // Mass Assigment
    // Set Field table who fillable 
    protected $fillable = ['id','nama', 'tanggal_mulai', 'tanggal_target', 'tanggal_selesai'];
    protected $hidden = ['id'];
}

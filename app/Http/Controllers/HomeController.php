<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Call all table from model Project.php 
        $projects = Project::all();
        // show view projects.blade.php from folder E 
        // passing data to view
        return view('home',['projects'=>$projects]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// method to call Project.php from folder App 
use App\Project;

class ProjectController extends Controller
{
    // Query Builder 
    // public function index()
    // {
    //     // Mengambil data projects dari table projects sebanyak 10 saja
    //     $projects = DB::table('projects')->paginate(2);
    //     // Mengirim data projects ke view index
    //     return view('projects.index',['projects' => $projects]);
    // }

    // public function showCreate()
    // {
    //     return view('projects.create');
    // }

    // public function create(Request $request)
    // {
    //     // Query menambahkan data ke database
    //     DB::table('projects')->insert([
    //         'nama' => $request -> nama,
    //         'tanggal_mulai' => $request -> tanggal_mulai,
    //         'tanggal_target' => $request -> tanggal_target,
    //         'tanggal_selesai' => $request -> tanggal_selesai
    //     ]);
    //     // Mengarahkan ke /projects setelah menambah data ke database
    //     return redirect('/projects');
    // }

    // public function showUpdate($projectId) 
    // {
    //     // Mengambil data dari database project berdasar id yang dipilih
    //     $projects = DB::table('projects') -> where('id',$projectId) -> get();

    //     // Passing data projects yang didapat dari view update.blade.php
    //     return view('projects.update',['projects' => $projects]);
    // }

    // public function update(Request $request)
    // {
    //     DB::table('projects') -> where('id', $request->id) -> update([
    //         'id' => $request -> id,
    //         'nama' => $request -> nama,
    //         'tanggal_mulai' => $request -> tanggal_mulai,
    //         'tanggal_target' => $request -> tanggal_target,
    //         'tanggal_selesai' => $request -> tanggal_selesai
    //     ]);
    //     // Mengarahkan ke /projects
    //     return redirect('/projects');
    // }

    // public function delete($projectId)
    // {
    //     // Menghapus database berdasarkan id 
    //     DB::table('projects') ->where('id',$projectId) ->delete();

    //     // ke halaman awal 
    //     return redirect('/projects');
    // }



                // Redirect Login
    public function login()
    {
        
        return redirect('/login');
    }


    
                // Eloquent
    // Read data from database
    public function index()
    {
        // call all table from model Project.php 
        $projects=Project::all();
        // show view projects.blade.php from folder E 
        // passing data to view
        return view('E.projects',['projects'=>$projects]);
    }

    // Form Add
    public function formAdd()
    {
        // show view formAdd.blade.php from folder E 
        return view('E.formAdd');
    }
    // Adding data to database
    public function add(Request $request)
    {
        // function to filled field like you want
        $this->validate($request,[
            'nama' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_target' => 'required', 
            'tanggal_selesai' => 'required'
        ]);

        // Create data to database
        Project::create([
            'nama' => $request -> nama,
            'tanggal_mulai' => $request -> tanggal_mulai,
            'tanggal_target' => $request -> tanggal_target,
            'tanggal_selesai' => $request -> tanggal_selesai
        ]);

        // go to /projects-Eloquent
        return redirect('/home');
    } 

    // Form Edit
    public function formEdit($projectId)
    {
        // Search project sesuai dengan projectId
        $projects = Project::find($projectId);
        

        // Show view --> formEdit.blade.php from folder E
        return view('E.formEdit',['projects' => $projects]);
    } 
    // Edit data to database
    public function edit($projectId, Request $request)
    { 
        // Bring warning when null form 
        $this->validate($request,[
            'nama' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_target' => 'required',
            'tanggal_selesai' => 'required'
        ]);

        // Save to database
        $projects = Project::find($request->id);
        $projects -> nama = $request -> nama;
        $projects -> tanggal_mulai = $request -> tanggal_mulai;
        $projects -> tanggal_target = $request -> tanggal_target;
        $projects -> tanggal_selesai = $request -> tanggal_selesai;
        $projects -> save();   

        return redirect ('/home');
        
    }

    // Delete from database
    public function delete($projectId)
    {
        $projects = Project::find($projectId);
        $projects->delete();
        return redirect('/home');
    }

}

